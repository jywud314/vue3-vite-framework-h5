import createAxios from '../axios.decoration';
const request = createAxios();

export function testget(params) {
  return request({
    url: 'test',
    method: 'get',
    params
  });
}

export function testpost(data) {
  return request({
    url: 'upload',
    method: 'post',
    data
  });
}

export function testpostform(data) {
  return request({
    url: 'process/save',
    method: 'post',
    isPostform: true,
    data
  });
}
