import { ref, computed } from 'vue';
import { defineStore } from 'pinia';

export const useCounterStore = defineStore(
  'counterStore',
  () => {
    let count = ref(10);
    let doubleCount = computed(() => count.value * 2);

    let increment = () => {
      count.value++;
    };

    return { count, doubleCount, increment };
  },
  {
    persist: {
      enabled: true,
      strategies: [
        {
          storage: sessionStorage
          // paths: ['id'],//指定要长久化的字段
        }
      ]
    }
  }
);
