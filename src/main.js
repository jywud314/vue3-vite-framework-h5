import 'amfe-flexible';
import { createApp } from 'vue';
import { createPinia } from 'pinia';
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate';

import App from './App.vue';
import router from './router';
// Toast 函数组件
import 'vant/es/toast/style';
import 'vant/es/dialog/style';
import 'vant/es/notify/style';
import 'vant/es/image-preview/style';
// import '@/assets/font/iconfont.css';
import './assets/style/base.less';
import './assets/style/vant-reset.less';
const app = createApp(App);
const pinia = createPinia();

pinia.use(piniaPluginPersistedstate);
app.use(pinia);
app.use(router);

// console.log(app.config.globalProperties);
// window.$router = router
app.mount('#app');

if (import.meta.env.VITE_APP_ENV === 'development') {
  import('vconsole').then(({ default: VConsole }) => {
    // eslint-disable-next-line no-new
    new VConsole();
  });
}
