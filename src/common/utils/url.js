/**
 * get请求query转为Object
 * @param query 请求的值query
 *
 */
export const getUrlParams = (query = window.location.search.slice(1)) => {
  query = query || (/\?[/\u4e00-\u9fa5_\-a-zA-Z0-9.=&]+/.exec(location.href) || [''])[0].slice(1);

  if (typeof query !== 'string') throw new Error('query must be a string');

  let obj = {};

  query.split('&').forEach((query) => {
    let [key, value] = query.split('=');
    obj[key] = decodeURI(value);
  });

  return obj;
};

/**
 * @desc 获取search某一个值得value
 * @param key search的key值
 *
 */
export const getUrlParam = (key) => getUrlParams()[key];
