/* eslint-disable no-undef */
import { fileURLToPath, URL } from 'node:url';
import { resolve } from 'path';
import { defineConfig, loadEnv } from 'vite';
import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';
// import legacyPlugin from '@vitejs/plugin-legacy';
// import pxtovw from 'postcss-px-to-viewport';
import pxtorem from 'postcss-pxtorem';
import Components from 'unplugin-vue-components/vite';
import { VantResolver } from 'unplugin-vue-components/resolvers';

export default ({ mode }) => {
  // // 获取.env文件里定义的环境变量
  const env = loadEnv(mode, process.cwd());
  return defineConfig({
    base: env.VITE_APP_BASE_URL,
    css: {
      preprocessorOptions: {
        less: {
          javascriptEnabled: true,
          additionalData: `@import "${resolve(__dirname, 'src/assets/style/common.less')}";`
        }
      },
      postcss: {
        plugins: [
          pxtorem({
            rootValue({ file }) {
              return file.indexOf('vant') !== -1 ? 37.5 : 75;
            },
            unitPrecision: 4,
            propList: ['*'],
            replace: !!env.VITE_APP_ENV === 'production',
            minPixelValue: 1
          })
          /* pxtovw({
            viewportWidth: 375, // UI设计稿的宽度
            unitToConvert: 'px', // 要转化的单位
            unitPrecision: 4, // 转换后的精度，即小数点位数
            propList: ['*'], // 指定转换的css属性的单位，*代表全部css属性的单位都进行转换
            viewportUnit: 'vw', // 指定需要转换成的视窗单位，默认vw
            selectorBlackList: [/node_modules/], // 指定不转换为视窗单位的类名，
            minPixelValue: 1, // 默认值1，小于或等于1px则不进行转换
            mediaQuery: true, // 是否在媒体查询的css代码中也进行转换，默认false
            replace: true, // 是否转换后直接更换属性值
            // exclude: [/node_modules/], // 设置忽略文件，用正则做目录名匹配
            landscape: false // 是否处理横屏情况
          }) */
        ]
      }
    },
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url)),
        '@c': fileURLToPath(new URL('./src/components', import.meta.url)),
        '@api': fileURLToPath(new URL('./src/interface/api', import.meta.url))
      }
    },
    server: {
      host: true, // 监听所有地址
      port: 5174, //本地端口号
      proxy: {
        //配置代理服务器
        '/api': {
          target: 'http://api.test.uat',
          rewrite: (path) => path.replace(/^\/api/, ''),
          changeOrigin: true //允许跨域
        }
      }
    },
    plugins: [
      vue(),
      vueJsx(),
      Components({
        resolvers: [VantResolver()]
      })
      // legacyPlugin({ // 兼容低版本浏览器
      //   targets: ['chrome 60'] // 需要兼容的目标列表，可以设置多个
      // })
    ],
    build: {
      outDir: 'dist', // 打包文件的输出目录
      assetsDir: 'assets', // 静态资源的存放目录
      assetsInlineLimit: 4096 // 图片转 base64 编码的阈值
    }
  });
};
